package com.ppetrov.youlocal.pojo;

import org.json.JSONObject;

/**
 * Created by ppetrov on 4/2/16.
 */
public abstract class BasePojo {
    protected BasePojo() {

    }

    abstract JSONObject toJson();
}
