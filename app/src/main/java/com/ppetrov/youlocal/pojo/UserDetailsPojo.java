package com.ppetrov.youlocal.pojo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ppetrov on 4/2/16.
 */
public class UserDetailsPojo extends BasePojo {

    public static final String AVATAR_ORIGINAL = "avatarOriginal";
    public static final String RADIUS = "radius";
    public static final String SUCCESS = "success";
    public static final String FULLNAME_LOWER = "fullname_lower";
    public static final String GENDER = "gender";
    public static final String ABOUTME = "aboutMe";
    public static final String AGES = "ages";
    public static final String COUNTS = "counts";
    public static final String LONGITUDE = "longitude";
    public static final String AVATAR = "avatar";
    public static final String LAST_UPDATE_DATE = "lastUpdateDate";
    public static final String TOKEN = "token";
    public static final String BIRTHDAY = "birthday";
    public static final String LOCATION_ID = "locationId";
    public static final String BIRTHDAY_PUBLIC = "birthday_public";
    public static final String LATITUDE = "latitude";
    public static final String PHONE_NUMBER = "phonenumber";
    public static final String FULL_NAME = "fullname";
    public static final String EMAIL = "email";
    public static final String USER_ID = "userId";
    public static final String NAME = "name";

    String avatarOriginal;
    double radius;
    String success;
    String fullnameLower;
    int gender;
    String aboutMe;
    int ages;
    UserDetailsPojo.CountsPojo counts;
    double longitude;
    String avatar;
    String lastUpdateDate;
    String token;
    String birthday;
    String locationId;
    boolean birthdayPublic;
    double latitude;
    String phonenumber;
    String fullname;
    String email;
    String userId;
    String name;

    public String getAvatarOriginal() {
        return avatarOriginal;
    }

    public double getRadius() {
        return radius;
    }

    public String getSuccess() {
        return success;
    }

    public String getFullnameLower() {
        return fullnameLower;
    }

    public int getGender() {
        return gender;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public int getAges() {
        return ages;
    }

    public CountsPojo getCounts() {
        return counts;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public String getToken() {
        return token;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getLocationId() {
        return locationId;
    }

    public boolean isBirthdayPublic() {
        return birthdayPublic;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public String getFullname() {
        return fullname;
    }

    public String getEmail() {
        return email;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public UserDetailsPojo(JSONObject jsonObject) throws JSONException {
        avatarOriginal = jsonObject.getString(AVATAR_ORIGINAL);
        radius = jsonObject.getDouble(RADIUS);
        success = jsonObject.getString(SUCCESS);
        fullnameLower = jsonObject.getString(FULLNAME_LOWER);
        gender = jsonObject.getInt(GENDER);
        aboutMe = jsonObject.getString(ABOUTME);
        ages = jsonObject.getInt(AGES);
        counts = new CountsPojo(jsonObject.getJSONObject(COUNTS));
        longitude = jsonObject.getDouble(LONGITUDE);
        avatar = jsonObject.getString(AVATAR);
        lastUpdateDate = jsonObject.getString(LAST_UPDATE_DATE);
        token = jsonObject.getString(TOKEN);
        birthday = jsonObject.getString(BIRTHDAY);
        locationId = jsonObject.getString(LOCATION_ID);
        birthdayPublic = jsonObject.getBoolean(BIRTHDAY_PUBLIC);
        latitude = jsonObject.getDouble(LATITUDE);
        phonenumber = jsonObject.getString(PHONE_NUMBER);
        fullname = jsonObject.getString(FULL_NAME);
        email = jsonObject.getString(EMAIL);
        userId = jsonObject.getString(USER_ID);
        name = jsonObject.getString(NAME);

    }

    @Override
    JSONObject toJson() {
        //stub
        return new JSONObject();
    }

    public class CountsPojo extends BasePojo {
        public static final String LOCATIONS = "locations";

        public int locations;

        public int getLocations() {
            return locations;
        }

        public CountsPojo(JSONObject jsonObject) throws JSONException {
            locations = jsonObject.getInt(LOCATIONS);
        }

        @Override
        JSONObject toJson() {
            //stub
            return new JSONObject();
        }
    }

}
