package com.ppetrov.youlocal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ppetrov.youlocal.pojo.UserDetailsPojo;
import com.ppetrov.youlocal.rest.RestClient;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.Response;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final String URL = "https://www.youlocalapp.com/oauth2/2.0/signin";

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private ImageView mLogoImageView;
    private TextView mForgottenPasswordTv;
    private Button mEmailSignInButton;

    private boolean error;

    private List<Animation> animations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Fade in animation for the logo
        mLogoImageView = (ImageView) findViewById(R.id.imageView);
        Animation logoAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fade_in);
        logoAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLogoImageView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mLogoImageView.setAnimation(logoAnimation);

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((Button) view).getText().equals(getResources().getString(R.string.reset))) {
                    //noop
                } else {
                    attemptLogin();
                }
            }
        });

        mForgottenPasswordTv = (TextView) findViewById(R.id.forgotten_password);
        mForgottenPasswordTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPasswordView.getVisibility() == View.VISIBLE) {
                    mPasswordView.setVisibility(View.GONE);
                    mForgottenPasswordTv.setText(R.string.back_to_login);
                    mEmailSignInButton.setText(R.string.reset);
                } else {
                    mPasswordView.setVisibility(View.VISIBLE);
                    mForgottenPasswordTv.setText(R.string.forgotten_password);
                    mEmailSignInButton.setText(R.string.action_sign_in);
                }
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        hideViews();
        prepareAnimations();
        logoAnimation.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void hideViews() {
        mLogoImageView.setVisibility(View.INVISIBLE);
        mPasswordView.setVisibility(View.INVISIBLE);
        mEmailView.setVisibility(View.INVISIBLE);
        mForgottenPasswordTv.setVisibility(View.INVISIBLE);

    }

    public void prepareAnimations() {
        Animation animation;

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_slide_in_left);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mEmailView.setVisibility(View.VISIBLE);
                TextInputLayout layout = (TextInputLayout) findViewById(R.id.email_layout);
                layout.setHint(getResources().getString(R.string.prompt_email));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mEmailView.setAnimation(animation);
        animations.add(animation);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_slide_in_right);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mPasswordView.setVisibility(View.VISIBLE);
                TextInputLayout layout = (TextInputLayout) findViewById(R.id.password_layout);
                layout.setHint(getResources().getString(R.string.prompt_password));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mPasswordView.setAnimation(animation);
        animations.add(animation);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_slide_in_left);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mEmailSignInButton.setVisibility(View.VISIBLE);
                mForgottenPasswordTv.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mEmailSignInButton.setAnimation(animation);
        animations.add(animation);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        // Check for a valid email address.
        if (TextUtils.isEmpty(email) || !isEmailValid(email)) {
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_vertical_shake);
            TextInputLayout emailLayout = (TextInputLayout) findViewById(R.id.email_layout);
            emailLayout.clearAnimation();
            emailLayout.setAnimation(animation);
            animation.start();
            return;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_vertical_shake);
            TextInputLayout passwordLayout = (TextInputLayout) findViewById(R.id.password_layout);
            passwordLayout.clearAnimation();
            passwordLayout.setAnimation(animation);
            animation.start();
            return;
        }


        // Show a progress spinner, and kick off a background task to
        // perform the user login attempt.
        showProgress(true);
        mAuthTask = new UserLoginTask(email, password);
        mAuthTask.execute((Void) null);
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        private final String TAG = UserLoginTask.class.getSimpleName();

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        Response postResponse;
        JSONObject response;

        @Override
        protected Boolean doInBackground(Void... params) {
            RestClient client = new RestClient(URL);
            client.setPostParameters("email", mEmail);
            client.setPostParameters("password", mPassword);
            try {
                postResponse = client.post();
                if (postResponse == null
                        || postResponse.getEntityAsText() == null
                        || postResponse.getEntityAsText().length() == 0
                        || postResponse.getStatus().getCode() != 200) {
                    return false;
                }
                response = new JSONObject(postResponse.getEntityAsText());
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            if (success) {
                if (response != null) {
                    Log.d(TAG, response.toString());
                    try {
                        displayProfileDialog(new UserDetailsPojo(response));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Error")
                        .setMessage("Login failed: " + postResponse.getStatus().getCode())
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                showProgress(false);
                            }
                        });
                builder.create();
                builder.show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        interface DownloadImageTaskCallback {
            void downloadCompleted();
        }

        ImageView bmImage;
        DownloadImageTaskCallback callback;

        public DownloadImageTask(ImageView bmImage, DownloadImageTaskCallback callback) {
            this.bmImage = bmImage;
            this.callback = callback;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            callback.downloadCompleted();
        }
    }

    private void displayProfileDialog(UserDetailsPojo userDetailsPojo) throws JSONException {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_profile, null);
        builder.setView(dialogView);
        final AlertDialog profileDialog = builder.create();
        profileDialog.getWindow().setLayout(Math.round(Resources.getSystem().getDisplayMetrics().widthPixels * 0.8f),
                Math.round(Resources.getSystem().getDisplayMetrics().heightPixels * 0.85f));


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            profileDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    View view = profileDialog.findViewById(R.id.profile_dialog_root);
                    int centerX = view.getWidth() / 2;
                    int centerY = view.getHeight() / 2;
                    float startRadius = 20;
                    float endRadius = view.getHeight();
                    Animator animator;
                    animator = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, startRadius, endRadius);
                    animator.setDuration(1500);
                    animator.start();
                }
            });
        }

        ImageView dialogImageView = (ImageView) dialogView.findViewById(R.id.profile_image);
        TextView usernameTextView = (TextView) dialogView.findViewById(R.id.profile_username);
        TextView userDescriptionTextView = (TextView) dialogView.findViewById(R.id.profile_user_description);

        usernameTextView.setText(userDetailsPojo.getFullname());
        userDescriptionTextView.setText(userDetailsPojo.getAboutMe());
        DownloadImageTask task = new DownloadImageTask(dialogImageView, new DownloadImageTask.DownloadImageTaskCallback() {
            @Override
            public void downloadCompleted() {
                profileDialog.show();
                showProgress(false);
            }
        });
        task.execute(userDetailsPojo.getAvatar());
    }
}

