package com.ppetrov.youlocal.rest;

import org.json.JSONException;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Form;
import org.restlet.data.Method;
import org.restlet.data.Protocol;
import org.restlet.resource.ClientResource;

/**
 * Created by ppetrov on 3/31/16.
 */
public class RestClient {
    public static final String TAG = RestClient.class.getSimpleName();

    ClientResource clientResource;
    String URL;
    Form form;

    public RestClient(String URL) {
        this.URL = URL;
        clientResource = new ClientResource(URL);
        clientResource.setProtocol(Protocol.HTTPS);
    }

    public RestClient(String URL, String login, String password) {
        this(URL);
        clientResource.setChallengeResponse(ChallengeScheme.HTTP_BASIC, login, password);
    }

    public void setQueryParameters(String key, String value) {
        clientResource.setQueryValue(key, value);
    }

    public void setPostParameters(String key, String value) {
        if(form == null) {
            form = new Form();
        }
        form.add(key, value);
    }

    public Response post() throws JSONException {
        Request request = new Request();
        request.setResourceRef(URL);
        request.setMethod(Method.POST);
        request.setEntity(form.getWebRepresentation());
        Response response = clientResource.handleOutbound(request);
        return response;
    }
}
